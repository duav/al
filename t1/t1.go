package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	//"sync"
)

//func parseString(wg *sync.WaitGroup, c chan string, c2 chan string) {
func parseString(c chan string, c2 chan string) {
	//defer wg.Done()

	c1 := make(chan string, 5)
	for i := range c {
		//wg.Add(1)
		c1 <- "Start"
		//go parseAndPrintOut(i, wg, c1)
		go parseAndPrintOut(i, c1)
		//_ = <-c1
	}
	c2 <- "Done"
} 

//func parseAndPrintOut(i string, wg *sync.WaitGroup, c1 chan string) {
func parseAndPrintOut(i string, c1 chan string) {
	//defer wg.Done()
    
	resp, err := http.Get(i)
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}

	numberOfTags := strings.Count(string(body), "<div")
	fmt.Printf("%s number of tags: %d\n", i, numberOfTags)
	_ = <- c1
}

// func getURLString(wg *sync.WaitGroup, c chan string) {
func getURLString(c chan string) {

	// defer wg.Done()

	urlFile, err := os.Open("./t1/url.txt")
	if err != nil {
		log.Fatal(err)
	}

	defer urlFile.Close()

	scanner := bufio.NewScanner(urlFile)
	for scanner.Scan() {
		c <- scanner.Text()
	}
	close(c)
}

func main() {

	//var wg sync.WaitGroup

	c := make(chan string, 5)
	c2 := make(chan string)

	//wg.Add(2)
  	// go getURLString(&wg, c)
	// go parseString(&wg, c, c2)
	go getURLString(c)
	go parseString(c, c2)

	<- c2
	//wg.Wait()

}
