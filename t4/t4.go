package main

import (
	"fmt"
)

func sliceBySliceReverse(someString string, sliceLenth int) string {

	var stringSlice string

	outputString := ""
	stringLenth := len(someString)

	for i := 0; i < stringLenth; {

		if i+sliceLenth >= stringLenth {
			stringSlice = someString[i:]
			i = stringLenth
		} else {
			stringSlice = someString[i : i+sliceLenth]
			i += sliceLenth
		}

		for ind := len(stringSlice) - 1; ind >= 0; ind-- {
			outputString += string(stringSlice[ind])
		}
	}

	return outputString

}

func main() {

	var inputString string = "123456789"

	// fmt.Println("Enter a string:")
	// fmt.Fscan(os.Stdin, &inputString)

	fmt.Printf("%s\n", sliceBySliceReverse(inputString, 5))

}
