package main

import (
	"bytes"
	"io"
	"reflect"
	"testing"
)

func Test_decode(t *testing.T) {
	type args struct {
		r io.Reader
	}
	tests := []struct {
		name    string
		args    args
		wantV   *features
		wantErr bool
	}{
		{
			name: "decode",
			args: args{bytes.NewReader([]byte(
				`<ValCurs Date="05.12.2019" name="Foreign Currency Market">
					<Valute ID="R01235">
						<NumCode>840</NumCode>
						<CharCode>USD</CharCode>
						<Nominal>1</Nominal>
						<Name>Доллар США</Name>
						<Value>64,1948</Value>
					</Valute>
					<Valute ID="R01239">
						<NumCode>978</NumCode>
						<CharCode>EUR</CharCode>
						<Nominal>1</Nominal>
						<Name>Евро</Name>
						<Value>71,1086</Value>
					</Valute>
				</ValCurs>`),
			)},
			wantV: &features{
				CharCode: []string{"USD", "EUR"},
				NumCode:  []string{"840", "978"},
				Nominal:  []int{1, 1},
				Name:     []string{"Доллар США", "Евро"},
				Rate:     []string{"64,1948", "71,1086"},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotV, err := decode(tt.args.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("decode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotV, tt.wantV) {
				t.Errorf("decode() = %v, want %v", gotV, tt.wantV)
			}
		})
	}
}

func Test_countRate(t *testing.T) {
	type args struct {
		v *features
	}
	tests := []struct {
		name    string
		args    args
		want    []ratePair
		wantErr bool
	}{
		{
			name: "simple test",
			args: args{
				&features{
					CharCode: []string{"USD", "RUB"},
					NumCode:  []string{"840", "643"},
					Nominal:  []int{1, 1},
					Name:     []string{"Доллар США", "Российский Рубль"},
					Rate:     []string{"64,1948", "1"},
				},
			},
			want: []ratePair{
				ratePair{
					CodeFrom:     "840",
					CharCodeFrom: "USD",
					NameFrom:     "Доллар США",
					CodeTo:       "840",
					CharCodeTo:   "USD",
					NameTo:       "Доллар США",
					rate:         1.0},
				ratePair{
					CodeFrom:     "840",
					CharCodeFrom: "USD",
					NameFrom:     "Доллар США",
					CodeTo:       "643",
					CharCodeTo:   "RUB",
					NameTo:       "Российский Рубль",
					rate:         64.1948,
				},
				ratePair{
					CodeFrom:     "643",
					CharCodeFrom: "RUB",
					NameFrom:     "Российский Рубль",
					CodeTo:       "840",
					CharCodeTo:   "USD",
					NameTo:       "Доллар США",
					rate:         0.015577585723454235,
				},
				ratePair{
					CodeFrom:     "643",
					CharCodeFrom: "RUB",
					NameFrom:     "Российский Рубль",
					CodeTo:       "643",
					CharCodeTo:   "RUB",
					NameTo:       "Российский Рубль",
					rate:         1.0,
				},
			},
			wantErr: false,
		},
		{
			name: "cross test",
			args: args{
				&features{
					CharCode: []string{"USD", "ФЫЧ"},
					NumCode:  []string{"840", "1001"},
					Nominal:  []int{10, 101},
					Name:     []string{"Доллар США", "Валюта Фычей"},
					Rate:     []string{"641,948", "111,111"},
				},
			},
			want: []ratePair{
				ratePair{
					CodeFrom:     "840",
					CharCodeFrom: "USD",
					NameFrom:     "Доллар США",
					CodeTo:       "840",
					CharCodeTo:   "USD",
					NameTo:       "Доллар США",
					rate:         1.0},
				ratePair{
					CodeFrom:     "840",
					CharCodeFrom: "USD",
					NameFrom:     "Доллар США",
					CodeTo:       "1001",
					CharCodeTo:   "ФЫЧ",
					NameTo:       "Валюта Фычей",
					rate:         58.35313155313155,
				},
				ratePair{
					CodeFrom:     "1001",
					CharCodeFrom: "ФЫЧ",
					NameFrom:     "Валюта Фычей",
					CodeTo:       "840",
					CharCodeTo:   "USD",
					NameTo:       "Доллар США",
					rate:         0.01713704086454182,
				},
				ratePair{
					CodeFrom:     "1001",
					CharCodeFrom: "ФЫЧ",
					NameFrom:     "Валюта Фычей",
					CodeTo:       "1001",
					CharCodeTo:   "ФЫЧ",
					NameTo:       "Валюта Фычей",
					rate:         1.0,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countRate(tt.args.v)
			if (err != nil) != tt.wantErr {
				t.Errorf("countRate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countRate() = %v, want %v", got, tt.want)
			}
		})
	}
}
