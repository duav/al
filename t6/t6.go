package main

import (
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"

	"golang.org/x/net/html/charset"
)

// СТРУКТУРЫ

type ratePair struct {
	CodeFrom     string
	CharCodeFrom string
	NameFrom     string
	CodeTo       string
	CharCodeTo   string
	NameTo       string
	rate         float64
}

type features struct {
	CharCode []string `xml:"Valute>CharCode"`
	NumCode  []string `xml:"Valute>NumCode"`
	Nominal  []int    `xml:"Valute>Nominal"`
	Name     []string `xml:"Valute>Name"`
	Rate     []string `xml:"Valute>Value"`
}

// ФУНКЦИЯ получения кросс-курсов валют
func getRate() ([]ratePair, error) {
	//получаем данные
	resp, err := http.Get("http://www.cbr.ru/scripts/XML_daily.asp")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	v, err := decode(resp.Body)
	if err != nil {
		return nil, err
	}

	addRub(v)

	return countRate(v)
}

func addRub(v *features) {
	v.CharCode = append(v.CharCode, "RUB")
	v.Name = append(v.Name, "Российский Рубль")
	v.NumCode = append(v.NumCode, "643")
	v.Nominal = append(v.Nominal, 1)
	v.Rate = append(v.Rate, "1")
}

func decode(r io.Reader) (*features, error) {
	var v features

	// парсим данные
	decoder := xml.NewDecoder(r)
	decoder.CharsetReader = charset.NewReaderLabel
	err := decoder.Decode(&v)
	if err != nil {
		return nil, err
	}

	return &v, nil
}

func countRate(v *features) ([]ratePair, error) {
	count := len(v.CharCode)
	returnPack := make([]ratePair, count*count)

	var transitCell ratePair
	counter := 0

	for ind := 0; ind < count; ind++ {
		transitCell.CodeFrom = v.NumCode[ind]
		transitCell.CharCodeFrom = v.CharCode[ind]
		transitCell.NameFrom = v.Name[ind]

		strRate := strings.Replace(v.Rate[ind], ",", ".", -1)
		numbRateFrom, err := strconv.ParseFloat(strRate, 64)
		if err != nil {
			return nil, err
		}

		for i := 0; i < count; i++ {
			transitCell.CodeTo = v.NumCode[i]
			transitCell.CharCodeTo = v.CharCode[i]
			transitCell.NameTo = v.Name[i]

			strRateTo := strings.Replace(v.Rate[i], ",", ".", -1)
			numbRateTo, err := strconv.ParseFloat(strRateTo, 64)
			if err != nil {
				return nil, err
			}

			transitCell.rate = (numbRateFrom * float64(v.Nominal[i])) / (numbRateTo * float64(v.Nominal[ind]))

			returnPack[counter] = transitCell
			counter++
		}

	}

	return returnPack, nil
}

// ОСНОВНАЯ ФУНКЦИЯ

func main() {

	var ratePack []ratePair
	var err error

	ratePack, err = getRate()
	fmt.Printf("%+v\n", ratePack)
	fmt.Printf("%+v\n", err)

}
