package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"./service"
)

var newService *service.Service

func main() {

	newService = new(service.Service)

	http.HandleFunc("/", homeRouterHandler)  // установим роутер
	err := http.ListenAndServe(":8080", nil) // задаем слушать порт
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func homeRouterHandler(w http.ResponseWriter, r *http.Request) {

	currencies := make([]string, 0)

	r.ParseForm()       //анализ аргументов,
	fmt.Println(r.Form) // ввод информации о форме на стороне сервера
	fmt.Println("path", r.URL.Path)
	fmt.Println("scheme", r.URL.Scheme)
	fmt.Println(r.Form["url_long"])
	for k, v := range r.Form {
		fmt.Println("key:", k)
		fmt.Println("val:", strings.Join(v, ""))
	}

	if tempSlice, ok := r.Form["c"]; ok {
		if len(tempSlice) > 0 {
			splittedCurr := strings.Split(tempSlice[0], ",")
			for i := range splittedCurr {
				currencies = append(currencies, string(splittedCurr[i]))
			}
		}
	} else {
		http.Error(w, "Currency codes haven't been found.", http.StatusBadRequest)
		//w.WriteHeader(http.StatusBadRequest)
		return
	} // проверяем, передана ли какая-то валюта

	rates, err := newService.GetCrossRates(currencies)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		//w.WriteHeader(http.StatusInternalServerError)
		return
	}

	b, _ := json.MarshalIndent(rates, "", " ")
	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
	//fmt.Fprintf(w, string(b)) // отправляем данные на клиентскую сторону
}
