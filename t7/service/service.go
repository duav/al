package service

import (
	"fmt"
	"sync"
	"time"
)

//Service СТРУКТУРА полученных курсов валют с типом RatePair
type Service struct {
	allRates   []RatePair
	lastUpdate time.Time
	mu         sync.Mutex
}

//GetCrossRates Функция получает курсы заданных валют
func (s *Service) GetCrossRates(currencies []string) ([]Rate, error) {
	var err error

	s.mu.Lock()
	timeNow := time.Now()

	fmt.Printf("%+v\n", s.lastUpdate)
	fmt.Printf("%+v\n", timeNow.Sub(s.lastUpdate))

	// обновляем данные не чаще чем раз в 5 минут
	if timeNow.Sub(s.lastUpdate) > 5*time.Minute {
		s.allRates, err = GetRate()
		if err != nil {
			return nil, err
		}

		s.lastUpdate = timeNow
	}

	returnRates, err := s.getCrossRates(currencies)
	s.mu.Unlock()

	return returnRates, err

}

func (s *Service) getCrossRates(currencies []string) ([]Rate, error) {
	count := len(currencies)
	returnPack := make([]Rate, 0)

	if count == 0 {
		return returnPack, nil
	}

	currenciesPairs := make([]string, count*count)
	counter := 0
	for i := 0; i < count; i++ {
		for j := 0; j < count; j++ {
			currenciesPairs[counter] = currencies[i] + currencies[j]
			counter++
		}
	}

	counter = 0
	for i := 0; i < count*count; i++ {
		for ind := 0; ind < len(s.allRates); ind++ {
			if s.allRates[ind].CharCodeFrom+s.allRates[ind].CharCodeTo == currenciesPairs[i] {
				TransitRate := Rate{
					CharCodeFrom: s.allRates[ind].CharCodeFrom,
					CharCodeTo:   s.allRates[ind].CharCodeTo,
					Rate:         s.allRates[ind].rate,
				}

				returnPack = append(returnPack, TransitRate)
				counter++
			}
		}
	}

	return returnPack, nil
}
