package service

//Rate СТРУКТУРА для возвращения кросс-курсов валют
type Rate struct {
	CharCodeFrom string  `json:"codeFrom"`
	CharCodeTo   string  `json:"codeTo"`
	Rate         float64 `json:"rate"`
}

//CurrencyRate ИНТЕРФЕЙС содержит метод получения кросс-курсов заданых валют
type CurrencyRate interface {
	GetCrossRates(currencies []string) ([]Rate, error)
}
