package main

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
)

type rate struct {
	CodeFrom string  `json:"codeFrom"`
	CodeTo   string  `json:"codeTo"`
	Rate     float64 `json:"rate"`
}

func main() {
	db, err := gorm.Open("postgres", "user=postgres password=123523 dbname=postgres sslmode=disable")
	//db, err := gorm.Open("postgres", "host=127.0.0.1 sslmode=disable port=5432 dbname=gorm-test user=postgres password=password search_path=mybeautifullscheme")
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	db.LogMode(true)

	db.DropTableIfExists(&rate{})
	db.AutoMigrate(&rate{})

	// тобишь вот туть меняем на
	db.Create(&rate{CodeFrom: "RUB", CodeTo: "USD", Rate: 0.56732})

	var rateGet rate
	// выгружаем всю таблицу
	db.Find(&rateGet)
	fmt.Println(rateGet)

	// ищем по отбору
	var rateGet2 rate
	db.Where("code_from = ?", "RUB").Where("(code_to LIKE ?", "RBK").Or("code_to LIKE ?)", "USD").Find(&rateGet2)

	fmt.Println(rateGet2)
}
