package main

import (
	"bufio"
	"log"
	"os"
	"sync"
	"time"
)

// func printText(wg *sync.WaitGroup, c chan string, quit chan string) {
// 	defer wg.Done()
// 	timeout := time.After(3 * time.Second)
// 	cIn := make(chan string)
// 	for {
// 		select {
// 		case <-timeout:
// 			log.Println(" ")
// 			for temp := range c {
// 				cIn <- temp
// 				break
// 			}
// 		case ms := <-c:
// 			log.Println(ms)
// 			//time.Sleep(3 * time.Second)
// 			timeout = time.After(3 * time.Second)
// 		case ms := <-cIn:
// 			log.Println(ms)
// 			//time.Sleep(3 * time.Second)
// 			timeout = time.After(3 * time.Second)
// 		case <-quit:
// 			break
// 		}
// 	}
// }

func printText(wg *sync.WaitGroup, c chan string, quit chan string) {
	defer wg.Done()
	timeout := time.After(3 * time.Second)
	secs := 3
	for {
		select {
		case ms := <-c:
			log.Println(ms)
			time.Sleep(3 * time.Second)
			secs = 3
			timeout = time.After(-1 * time.Second)
		case <-timeout:
			if secs == 3 {
				log.Println(" ")
				timeout = time.After(1 * time.Second)
				secs = 1
			} else {
				timeout = time.After(1 * time.Second)
			}
		case <-quit:
			break
		}
	}
}

func main() {
	var wg sync.WaitGroup
	c := make(chan string)
	quit := make(chan string)

	urlFile, err := os.Open("./t2/url.txt")
	if err != nil {
		log.Fatal(err)
	}

	defer urlFile.Close()

	wg.Add(2)
	go printText(&wg, c, quit)

	scanner := bufio.NewScanner(urlFile)
	for scanner.Scan() {
		c <- scanner.Text()
		time.Sleep(1 * time.Second)
	}
	quit <- "done"
	close(c)
	wg.Done()
}
