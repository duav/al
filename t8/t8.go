package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	_ "github.com/lib/pq"
)

type currency struct {
	currencyCode     string
	currencyCharCode string
	currencyName     string
	rate             float32
}

type rate struct {
	CodeFrom string  `json:"codeFrom"`
	CodeTo   string  `json:"codeTo"`
	Rate     float64 `json:"rate"`
}

func main() {

	searchCurrency := "GBP,USD,SEK"
	httpString := "http://localhost:8080/rates?c=" + searchCurrency

	resp, err := http.Get(httpString)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	var m []rate
	if err := json.Unmarshal(body, &m); err != nil {
		panic(err)
	}

	db, err := sql.Open("postgres", "user=postgres password=123523 dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	// _, err = db.Exec("DROP TABLE IF EXISTS Currencies")
	// if err != nil {
	// 	log.Panic(err)
	// }

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS Currencies(
			codeFrom char(3),
			codeTo char(3), 
			rate decimal(6,4) NOT NULL,
			PRIMARY KEY(codeFrom, codeTo)
	)`)
	if err != nil {
		log.Panic(err)
	}

	// Вставляем данные
	for i := range m {

		result, err := db.Exec(`
		INSERT into Currencies
		(codeFrom, codeTo, rate) values
		($1, $2, $3)
		ON CONFLICT (codeFrom, codeTo) 
		DO UPDATE SET
		rate = $3`,
			m[i].CodeFrom, m[i].CodeTo, m[i].Rate)
		if err != nil {
			log.Panic(err)
		}

		fmt.Println(result.RowsAffected())

		// result, err := db.Exec(`UPDATE Currencies
		// set rate = $1
		// where codeFrom = $2 and codeTo = $3`,
		// 	m[i].Rate, m[i].CodeFrom, m[i].CodeTo)
		// if err != nil {
		// 	log.Panic(err)
		// }
		// strSent, _ := result.RowsAffected()

		// _, err = db.Exec(`
		// insert into Currencies
		// (codeFrom, codeTo, rate) values
		// ($1, $2, $3)`,
		// 	m[i].CodeFrom, m[i].CodeTo, m[i].Rate)
		// if err != nil {
		// 	log.Panic(err)
		// }
	}

	rows, err := db.Query("SELECT * from Currencies")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		r := rate{}
		err = rows.Scan(&r.CodeFrom, &r.CodeTo, &r.Rate)
		if err != nil {
			fmt.Println(err)
			continue
		}
		fmt.Println(r)
	}

	// _, err = db.Exec(`
	// 	insert into Currencies
	// 	(currencyCode, currencyCharCode, currencyName, rate) values
	// 	($1, $2, $3, $4)`,
	// 	"840", "USD", "Доллар США", 61.5333)
	// if err != nil {
	// 	log.Panic(err)
	// }
	// _, err = db.Exec("insert into Currencies (currencyCode, currencyCharCode, currencyName, rate) values ($1, $2, $3, $4)",
	// 	"978", "EUR", "Евро", 68.5358)
	// if err != nil {
	// 	log.Panic(err)
	// }
	// _, err = db.Exec("insert into Currencies (currencyCode, currencyCharCode, currencyName, rate) values ($1, $2, $3, $4)",
	// 	"156", "CNY", "Китайский Юань", 89.7105)
	// if err != nil {
	// 	log.Panic(err)
	// }

	// rows, err := db.Query("SELECT * FROM Currencies WHERE currencyCharCode = $1", searchCurrency)
	// if err != nil {
	// 	log.Panic(err)
	// }
	// defer rows.Close()

	// crs := make([]*currency, 0)
	// for rows.Next() {
	// 	bk := new(currency)
	// 	err := rows.Scan(&bk.currencyCode, &bk.currencyCharCode, &bk.currencyName, &bk.rate)
	// 	if err != nil {
	// 		log.Panic(err)
	// 	}
	// 	crs = append(crs, bk)
	// }

	// fmt.Println(crs)
	// if err = rows.Err(); err != nil {
	// 	log.Panic(err)
	// }

	// for _, bk := range crs {
	// 	fmt.Printf("%s, %s, %s, Rate%.2f\n", bk.currencyCode, bk.currencyCharCode, bk.currencyName, bk.rate)
	// }
}
