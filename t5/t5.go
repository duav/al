package main

import (
	"fmt"
	"os"
)

// СТРУКТУРЫ

type personData struct {
	age   int
	email string
}

// МЕТОДЫ

func (dataCell *personData) add() {
	var age int
	var email string

	// получаем данные
	fmt.Println("Enter an age:")
	fmt.Fscan(os.Stdin, &age)

	fmt.Println("Enter an email:")
	fmt.Fscan(os.Stdin, &email)

	dataCell.age = age
	dataCell.email = email
}

func (dataCell personData) get() {

	fmt.Println("age: ", dataCell.age)
	fmt.Println("email: ", dataCell.email)

}

// ОСНОВНАЯ ФУНКЦИЯ

func main() {
	var command string
	var name string

	dataBase := make(map[string]personData)

	for {
		fmt.Println("Enter  'add' to enter data. \n Enter 'get' to get data by name \n Enter 'quit' to quit the programm.")
		//ждем команду от пользователя
		fmt.Fscan(os.Stdin, &command)

		switch string(command) {
		case "quit":
			// команда завершает программу
			fmt.Println("Come again =)")
		case "add":
			fmt.Println("Enter a name:")
			fmt.Fscan(os.Stdin, &name)

			data := personData{}
			data.add()
			dataBase[string(name)] = data

			fmt.Println("Successfuly added.")
		case "get":
			// получаем данные по имени
			fmt.Println("Enter a name:")
			fmt.Fscan(os.Stdin, &name)

			if foundData, ok := dataBase[string(name)]; ok {
				foundData.get()
			} else {
				fmt.Println("There is no such name in data base.")
			}
		default:
			// нефик вводить всякую чухню
			fmt.Println("Don't know this command. \n Please try again.\n")
		}

		if string(command) == "quit" {
			break
		}

	}
}
