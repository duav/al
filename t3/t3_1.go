package main

import (
	"fmt"
	"os"
)

func main() {
	var input string
	var addString string
	var checkLetter string
	var letterCounter map[string]int

	letterCounter = make(map[string]int)

	fmt.Println("Enter  'add' to count letters. \n Enter 'get' to get total of a letter \n Enter 'all' to print the result \n Enter 'quit' to quit the programm.")
	for {
		//ждем команду от пользователя
		fmt.Fscan(os.Stdin, &input)

		switch string(input) {
		case "quit":
			// команда завершает программу
			fmt.Println("Come again =)")
		case "add":
			// считаем символы введенной строки
			fmt.Println("Enter a string:")
			fmt.Fscan(os.Stdin, &addString)
			for _, value := range addString {
				letterCounter[string(value)] += 1
			}
		case "get":
			// получаем посчитанное значение
			fmt.Println("Enter a letter:")
			fmt.Fscan(os.Stdin, &checkLetter)
			if count, ok := letterCounter[string(checkLetter)]; ok {
				fmt.Println(count)
			} else {
				fmt.Println("0")
			}
		case "all":
			for key, value := range letterCounter {
				fmt.Println(key, value)
			}
		default:
			// нефик вводить всякую чухню
			fmt.Printf("%v", "Don't know this command. \n Please try again.\n")
		}

		if string(input) == "quit" {
			break
		}

	}
}
