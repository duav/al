package main

import (
	"fmt"
	"os"
)

func main() {
	var input string
	var addString string
	var checkLetter string
	var letterCounter map[string]int

	letterCounter = make(map[string]int)

	for {
		//ждем команду от пользователя
		fmt.Println("Enter  'add' to count letters. \n Enter 'get' to get total of a letter \n Enter 'quit' to quit the programm.")
		fmt.Fscan(os.Stdin, &input)

		if string(input) == "quit" {
			// команда завершает программу
			fmt.Println("Come again =)")
			break
		} else if string(input) == "add" {
			// считаем символы введенной строки
			fmt.Println("Enter a string:")
			fmt.Fscan(os.Stdin, &addString)
			for _, value := range addString {
				letterCounter[string(value)] += 1
			}
		} else if string(input) == "get" {
			// получаем посчитанное значение
			fmt.Println("Enter a letter:")
			fmt.Fscan(os.Stdin, &checkLetter)
			if count, ok := letterCounter[string(checkLetter)]; ok {
				fmt.Println(count)
			} else {
				fmt.Println("0")
			}
		} else {
			// нефик вводить всякую чухню
			fmt.Println("Don't know this command. \n Please try again.\n")
		}

	}
}
